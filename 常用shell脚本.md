- 查询Llisten名字线程相关的信息，主要关注psr：CPU核心数

查询带Llisten关键字的线程的ppid，并将ppid以-e作为串联，可以作为后续grep命令的入参

```shell
ps -TA | grep -i Llisten | awk '{printf " -e %s ",$2}'
```

```
[root@openGauss119 gazelle_conf]# ps -TA | grep -i Llisten | sort
 498806  498849 pts/5    00:00:15 TPLlistener
 498806  498967 pts/5    00:00:16 TPLlistener
 498806  499089 pts/5    00:00:15 TPLlistener
 498806  499211 pts/5    00:00:16 TPLlistener
```

使用上面命令作为过滤入参，查询这些线程的具体信息。

```shell
ps -TA -o tid,pid,ppid,psr,cmd | grep ` ps -TA | grep -i Llisten | awk '{printf " -e %s ",$2}'`
```

结果输出：一些小结论：Listener线程运行的CPU是不固定的。

```shell
[root@openGauss119 gazelle_conf]# ps -TA -o tid,pid,ppid,psr,cmd | grep ` ps -TA | grep -i Llisten | awk '{printf " -e %s ",$2}'` | sort
 498849  498806  498805  70 gaussdb -D /usr1/zhangzhixian/data
 498967  498806  498805  74 gaussdb -D /usr1/zhangzhixian/data
 499089  498806  498805 101 gaussdb -D /usr1/zhangzhixian/data
 499211  498806  498805  27 gaussdb -D /usr1/zhangzhixian/data
 499649  499649  272653  48 grep --color=auto -e 498849 -e 498967 -e 499089 -e 499211
[root@openGauss119 gazelle_conf]# ps -TA -o tid,pid,ppid,psr,cmd | grep ` ps -TA | grep -i Llisten | awk '{printf " -e %s ",$2}'` | sort
 498849  498806  498805  86 gaussdb -D /usr1/zhangzhixian/data
 498967  498806  498805  93 gaussdb -D /usr1/zhangzhixian/data
 499089  498806  498805 119 gaussdb -D /usr1/zhangzhixian/data
 499211  498806  498805   1 gaussdb -D /usr1/zhangzhixian/data
 499658  499658  272653   2 grep --color=auto -e 498849 -e 498967 -e 499089 -e 499211
[root@openGauss119 gazelle_conf]# ps -TA -o tid,pid,ppid,psr,cmd | grep ` ps -TA | grep -i Llisten | awk '{printf " -e %s ",$2}'` | sort
 498849  498806  498805  87 gaussdb -D /usr1/zhangzhixian/data
 498967  498806  498805  80 gaussdb -D /usr1/zhangzhixian/data
 499089  498806  498805  97 gaussdb -D /usr1/zhangzhixian/data
 499211  498806  498805  15 gaussdb -D /usr1/zhangzhixian/data
 499670  499670  272653  12 grep --color=auto -e 498849 -e 498967 -e 499089 -e 499211
```





查询某个CPU上的线程信息

```shell
ps -TA -o tid,pid,ppid,psr,cmd | awk '{if ($4=='2') print $1 " "$5}' 
```

拼接成-e形式

```shell
grep ps -TA -o tid,pid,ppid,psr,cmd | awk '{if ($4=='2') printf " -e %s", $1}' 
```

查询线程名字

```
ps -TA | grep `ps -TA -o tid,pid,ppid,psr,cmd | awk '{if ($4=='2') printf " -e %s ", $1}' ` 
```

- 

```shell
#!/bin/bash
count=0
for (( ; ; )); do
        gsql -dtpcc1000 -p16520 -r -c "select *,${count} as count from dbe_perf.wait_events where type = 'LWLOCK_EVENT';" >> wait_event.txt
        let count++
        sleep 5
done
```

需要打开GUC参数

```shell
track_activities=off
enable_instr_track_wait=off
```

- xlog

打开

```
-- turn off
select * from gs_walwriter_flush_stat(-1);
-- turn on
select * from gs_walwriter_flush_stat(0);
-- query
select * from gs_walwriter_flush_stat(1);
-- reset
select * from gs_walwriter_flush_stat(2);
```

- perfui

```
perf record -m 400M -g -a -e cpu-cycles -e sched:sched_wakeup -e sched:sched_switch bash -c "sleep 0.20"
```

转换

```
perf script -F comm,tid,cpu,time,event,ip,sym,trace > perfui_202404020959.txt
```



`
