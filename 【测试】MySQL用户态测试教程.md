## 源码下载

```
wget https://downloads.mysql.com/archives/get/p/23/file/mysql-boost-8.0.20.tar.gz
wget https://github.com/kunpengcompute/mysql-server/releases/download/tp_v1.0.0/0001-SHARDED-LOCK-SYS.patch
wget https://github.com/kunpengcompute/mysql-server/releases/download/21.0.RC1.B031/0001-SCHED-AFFINITY.patch
wget https://github.com/kunpengcompute/mysql-server/releases/download/tp_v1.0.0/0002-LOCK-FREE-TRX-SYS.patch
wget https://github.com/kunpengcompute/mysql-server/releases/download/21.0.RC1.B031/0001-SCHED-AFFINITY.patch
tar -zxvf mysql-boost-8.0.20.tar.gz
```

## 环境构造

```
yum install libtirpc-devel -y
wget https://github.com/thkukuk/rpcsvc-proto/releases/download/v1.4.1/rpcsvc-proto-1.4.1.tar.xz
xz -d rpcsvc-proto-1.4.1.tar.xz
tar -xvf rpcsvc-proto-1.4.1.tar
cd rpcsvc-proto-1.4.1
./configure
make
make install
```

## 编译安装

```
DEST_PATH=/usr1/mysql/mysql-8.0.20/output
tar zxvf mysql-boost-8.0.20.tar.gz
cd mysql-8.0.20/
patch -p1 < ../0001-SHARDED-LOCK-SYS.patch
patch -p1 < ../0001-SCHED-AFFINITY.patch
patch -p1 < ../0002-LOCK-FREE-TRX-SYS.patch
cd cmake
make clean
cmake .. -DCMAKE_INSTALL_PREFIX=$DEST_PATH  -DWITH_BOOST=../boost -DDOWNLOAD_BOOST=1
make -j 64
make install
```

## 初始化数据库

```
ln -svf /usr1/mysql/data /data
mkdir -p /data/mysql
mkdir -p /data/mysql/data
mkdir -p /data/mysql/share
mkdir -p /data/mysql/tmp
mkdir -p /data/mysql/run
mkdir -p /data/mysql/log
#创建用户组
groupadd mysql
useradd -g mysql mysql
chown -R mysql:mysql /data
chown -R mysql:mysql /data/mysql/log/mysql.log
#初始化
echo "" > /data/mysql/log/mysql.log
rm -fr /data/mysql/data/*
$DEST_PATH/bin/mysqld  --defaults-file=/etc/my.cnf --user=root --initialize

#启动服务
$DEST_PATH/support-files/mysql.server start

#完成初始化后会随机生成一个密码，用其登录mysql
$DEST_PATH/bin/mysql -u root -p
alter user 'root'@'localhost' identified by '123456';
flush privileges;
use mysql;
update user set host='%' where user='root';
flush privileges;
create database tpcc;
quit
```

## benchmark工具配置

```
wget https://mirrors.huaweicloud.com/kunpeng/archive/kunpeng_solution/database/patch/benchmarksql5.0-for-mysql.zip
yum install -y java
unzip benchmarksql5.0-for-mysql.zip
cd benchmarksql5.0-for-mysql/run
chmod +x *.sh
```

对应路径：/usr1/tmp/zhangzhixian/tools/benchmarksql5.0-for-mysql

### 修改配置文件

```
db=mysql
driver=com.mysql.cj.jdbc.Driver
conn=jdbc:mysql://20.20.20.119:3306/tpcc?useSSL=false&useServerPrepStmts=true&useConfigs=maxPerformance&rewriteBatchedStatements=true
user=root
password=123456
profile=/etc/my.cnf
data=/data/mysql/data
backup=/data/mysql/backup

warehouses=1000
loadWorkers=100

terminals=300
//To run specified transactions per terminal- runMins must equal zero
runTxnsPerTerminal=0
//To run for specified minutes- runTxnsPerTerminal must equal zero
runMins=10
//Number of total transactions per minute
limitTxnsPerMin=1000000000

//Set to true to run in 4.x compatible mode. Set to false to use the
//entire configured database evenly.
terminalWarehouseFixed=true

//The following five values must add up to 100
//The default percentages of 45, 43, 4, 4 & 4 match the TPC-C spec
newOrderWeight=45
paymentWeight=43
orderStatusWeight=4
deliveryWeight=4
stockLevelWeight=4

// Directory name to create for collecting detailed result data.
// Comment this out to suppress.
resultDirectory=my_result_%tY-%tm-%td_%tH%tM%tS
//osCollectorScript=./misc/os_collector_linux.py
//osCollectorInterval=1
//osCollectorSSHAddr=user@dbhost
//osCollectorDevices=net_eth0 blk_sda=
```

### 初始化数据

需要调整删除外键：删除文件sql.mysql/foreignKeys.sql内的相关外键语句。

```
./runDatabaseBuild.sh props.conf
```

### 备份数据

后续可以通过清除恢复的方式，快速恢复数据。

```
cp /usr1/mysql/data/mysql /usr1/mysql/data/mysql_bak -r
```

## 性能测试

### benchmark测试文件

```
db=mysql
driver=com.mysql.cj.jdbc.Driver
conn=jdbc:mysql://20.20.20.119:3306/tpcc?useSSL=false&useServerPrepStmts=true&useConfigs=maxPerformance&rewriteBatchedStatements=true
user=root
password=123456
profile=/etc/my.cnf
data=/data/mysql/data
backup=/data/mysql/backup

warehouses=1000
loadWorkers=100

terminals=500
//To run specified transactions per terminal- runMins must equal zero
runTxnsPerTerminal=0
//To run for specified minutes- runTxnsPerTerminal must equal zero
runMins=20
//Number of total transactions per minute
limitTxnsPerMin=0

//Set to true to run in 4.x compatible mode. Set to false to use the
//entire configured database evenly.
terminalWarehouseFixed=true

//The following five values must add up to 100
//The default percentages of 45, 43, 4, 4 & 4 match the TPC-C spec
newOrderWeight=45
paymentWeight=43
orderStatusWeight=4
deliveryWeight=4
stockLevelWeight=4

// Directory name to create for collecting detailed result data.
// Comment this out to suppress.
// resultDirectory=my_result_%tY-%tm-%td_%tH%tM%tS
//osCollectorScript=./misc/os_collector_linux.py
//osCollectorInterval=1
//osCollectorSSHAddr=user@dbhost
//osCollectorDevices=net_eth0 blk_sda
```

### 内核态测试（基线）

1. 设置网卡中断，需要修改enp8s0这个名字以及网络中断数量4和cpulist（网络中断绑定到哪些CPU上）。目前设置为：
   1. 修改enp8s0网卡设置4个中断。
   2. 设置enp8s0的4个中断分别为：31 63 95  127。

```
ethtool -L enp8s0 combined 4
irq1=`cat /proc/interrupts| grep -E enp8s0 | head -n9 | awk -F ':' '{print $1}'`
cpulist=(31 63 95 127)
c=0
for irq in $irq1
do
	echo ${cpulist[c]} "->" $irq
	echo ${cpulist[c]} > /proc/irq/$irq/smp_affinity_list
	let "c++"
done

```

2. 数据库启动

```
numactl -C 0-30,32-62,64-94,96-126 $DEST_PATH/bin/mysqld --defaults-file=/etc/my.cnf --bind-address=20.20.20.119 &
```

3. 客户端

```
numactl -C 0-21,32-53,64-85,96-117 ./runBenchmark.sh props.conf
```

### 用户态测试（基线）

lstack.conf文件配置；vim /etc/gazelle/lstack.conf

```
# Copyright (c) Huawei Technologies Co., Ltd. 2020-2021. All rights reserved.
# gazelle is licensed under the Mulan PSL v2.
# You can use this software according to the terms and conditions of the Mulan PSL v2.
# You may obtain a copy of Mulan PSL v2 at:
#     http://license.coscl.org.cn/MulanPSL2
# THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
# PURPOSE.
# See the Mulan PSL v2 for more details.

dpdk_args=["--socket-mem", "3096,3096,3096,3096", "--huge-dir", "/mnt/hugepages-512M", "--proc-type", "primary", "--legacy-mem", "--map-perfect"]

use_ltran=0
kni_switch=0

low_power_mode=0
 
#needed mbuf count = tcp_conn_count * mbuf_count_per_conn
tcp_conn_count = 1500
mbuf_count_per_conn = 170

# send ring size, default is 32, max is 2048
send_ring_size = 32

# 0: when send ring full, send return
# 1: when send ring full, alloc mbuf from mempool to send data
expand_send_ring = 0
 
#protocol stack thread per loop params
#read data form protocol stack into recv_ring
read_connect_number = 4
#process rpc msg number
rpc_number = 4
#read nic pkts number
nic_read_number = 128

#each cpu core start a protocol stack thread.
#num_cpus="30,31,62,63,94,95,126,127"
#num_cpus="15,31,47,63,79,95,111,127"
#num_cpus="30,31,62,63,94,95,126,127"
#num_cpus="30,31,62,63,94,95,126,127"
num_cpus="31,63,95,127"
#num_cpus="30,31,63,94,95,127"
#num_cpus="29,30,31,61,62,63,93,94,95,125,126,127"

#app worker thread bind to numa in epoll/poll.
app_bind_numa=1
#app main thread affinity set by dpdk.
main_thread_affinity=0

host_addr="20.20.20.119"
mask_addr="255.255.255.0"
gateway_addr="20.20.20.1"
devices="78:b4:6a:40:16:31"

udp_enable=0
#0: use rss rule
#1: use tcp tuple rule to specify packet to nic queue
tuple_filter=0

#tuple_filter=1, below cfg valid
num_process=1
process_numa="0,1"
process_idx=0

#tuple_filer=0, below cfg valid
listen_shadow=1
#exclude cpu
#exclude_cpus="23,55,87,119,24,56,88,120,25,57,89,121,26,58,90,122,27,59,91,123,28,60,92,124,29,61,93,125,30,62,94,126"
#app_exclude_cpus="23-30,55-62,87-94,119-126"
#app_exclude_cpus="23,55,87,119,24,56,88,120,25,57,89,121,26,58,90,122,27,59,91,123,28,60,92,124,29,61,93,125,30,62,94,126"
#app_exclude_cpus="0"
unix_prefix="0"
```

2. 预配置：需要根据具体内容调整

```
echo 40 > /sys/kernel/mm/hugepages/hugepages-524288kB/nr_hugepages
mkdir /mnt/hugepages-512M
mount -t hugetlbfs -o pagesize=512M nodev /mnt/hugepages-512M


modprobe uio
insmod //usr/lib/modules/4.19.90-2012.4.0.0053.oe1.aarch64/extra/dpdk/igb_uio.ko
#insmod //usr/lib/modules/4.19.90-2012.4.0.0053.oe1.aarch64/extra/dpdk/igb_uio.ko

insmod /usr/lib/modules/4.19.90-2012.4.0.0053.oe1.aarch64/extra/dpdk/rte_kni.ko carrier="on"

modprobe vfio enable_unsafe_noiommu_mode=1
modprobe vfio-pci
```

3. 设置用户态接管

```
PCI=0000:08:00.0
NET_DEVICE=enp8s0
ifconfig $NET_DEVICE down
#dpdk-devbind -b igb_uio $PCI
sudo modprobe vfio enable_unsafe_noiommu_mode=1
sudo echo 1 > /sys/module/vfio/parameters/enable_unsafe_noiommu_mode
sudo dpdk-devbind -b vfio-pci $PCI
```

后面还原：

```
PCI=0000:08:00.0
NET_DEVICE=enp8s0
IP=20.20.20.119
sudo dpdk-devbind -u $PCI
sudo dpdk-devbind -b hinic $PCI
sudo ifconfig $NET_DEVICE $IP netmask 255.255.255.0
```

4. 启动

```
LD_PRELOAD=/usr/lib64/liblstack.so GAZELLE_BIND_PROCNAME=mysqld LSTACK_CONF_PATH=/etc/gazelle/lstack.conf $DEST_PATH/bin/mysqld --defaults-file=/etc/my.cnf --bind-address=20.20.20.119
```

5. TPCC测试

```
numactl -C 0-21,32-53,64-85,96-117 ./runBenchmark.sh props.conf
```

## 配置文件

vim /etc/my.cnf

```
[mysqld_safe]
log-error=/data/mysql/log/mysql.log
pid-file=/data/mysql/run/mysqld.pid

[client]
socket=/data/mysql/run/mysql.sock
default-character-set=utf8

[mysqld]
server-id=1
#log-error=/data/mysql/log/mysql.log
#basedir=/usr/local/mysql
socket=/data/mysql/run/mysql.sock
tmpdir=/data/mysql/tmp
datadir=/data/mysql/data
default_authentication_plugin=mysql_native_password
port=3306
user=root
#innodb_page_size=4k


max_connections=2000
back_log=4000
performance_schema=OFF
max_prepared_stmt_count=128000
#transaction_isolation=READ-COMMITTED
#skip-grant-tables

#file
innodb_file_per_table
innodb_log_file_size=2048M
innodb_log_files_in_group=32
innodb_open_files=10000
table_open_cache_instances=64

#buffers
innodb_buffer_pool_size=230G
innodb_buffer_pool_instances=16
innodb_log_buffer_size=2048M
innodb_undo_log_truncate=OFF

#tune
default_time_zone=+8:00
#innodb_numa_interleave=1
thread_cache_size=2000
sync_binlog=1
innodb_flush_log_at_trx_commit=1
innodb_use_native_aio=1
innodb_spin_wait_delay=180
innodb_sync_spin_loops=25
innodb_flush_method=O_DIRECT
innodb_io_capacity=30000
innodb_io_capacity_max=40000
innodb_lru_scan_depth=9000
innodb_page_cleaners=16
#innodb_spin_wait_pause_multiplier=25

#perf special
innodb_flush_neighbors=0
innodb_write_io_threads=1
innodb_read_io_threads=1
innodb_purge_threads=1

sql_mode=STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION,NO_AUTO_VALUE_ON_ZERO,STRICT_ALL_TABLES

log-bin=mysql-bin
skip_log_bin
ssl=0
table_open_cache=30000
max_connect_errors=2000
innodb_adaptive_hash_index=0

mysqlx=0

#sched_affinity_foreground_thread=0-21,24-42,46,48-69,72-94
#sched_affinity_log_writer=44
#sched_affinity_log_flusher=45
#sched_affinity_log_write_notifier=45
#sched_affinity_log_flush_notifier=45
#sched_affinity_log_closer=43
#sched_affinity_log_checkpointer=45
#sched_affinity_purge_coordinator=43

```

## 测试性能

其它测试方式：运行1分钟后，再运行10/20分钟（tpmTOTAL）

| 用户态/内核态 | 并发 | 用户态/内核态线程数量 | 10分钟     | 20分钟     | 30分钟     |
| ------------- | ---- | --------------------- | ---------- | ---------- | ---------- |
| 用户态        | 180  | 8                     | 1351421.75 | 1487484.4  |            |
| 用户态        | 240  | 8                     | 1287605.71 | 1427931.54 |            |
| 用户态        | 300  | 8                     | 1176352.03 | 1351863.81 |            |
| 用户态        | 400  | 8                     | 1081996.57 | 1306278.58 |            |
| 用户态        | 500  | 8                     |            | 1248282.72 |            |
| 用户态        | 600  | 8                     | 961251.51  |            | 1303408.64 |
| 内核态        | 180  | 8                     | 1294469.69 | 1394831.38 |            |
| 内核态        | 240  | 8                     | 1169495.99 | 1358981.29 |            |
| 内核态        | 300  | 8                     | 1103169.9  | 1311693.78 |            |
| 内核态        | 400  | 8                     | 1013552.53 | 1238796.97 |            |
| 内核态        | 500  | 8                     |            | 1182773.65 |            |
| 内核态        | 600  | 8                     | 931302.78  |            | 1225569.81 |

其它测试方式：运行1分钟后，再运行10/20分钟（tpmTOTAL）

| 用户态/内核态 | 并发 | 用户态/内核态线程数量 | 10分钟 | 20分钟     | 30分钟     |
| ------------- | ---- | --------------------- | ------ | ---------- | ---------- |
| 用户态        | 180  | 4                     |        | 1409538.19 |            |
| 用户态        | 600  | 4                     |        |            | 1305089.79 |
| 用户态        | 300  |                       |        |            |            |
| 用户态        |      |                       |        |            |            |
| 内核态        | 180  | 4                     |        | 1196448.7  |            |
| 内核态        | 600  | 4                     |        |            | 1145285.29 |
| 内核态        | 300  |                       |        |            |            |
| 内核态        | 400  |                       |        |            |            |

总结：

1. 在用户态/内核态处理线程设置为4时，180/600并发情况下用户态性能会有显著提升，大约有17.8%提升比例。
2. 在用户态/内核态处理线程设置为8时，此时网络瓶颈相对不高，用户态相对提升不明显。在并发数180情况下，大约有6%提升。在并发数500情况下，5%提升，在并发数400情况下，大概有3%左右提升。
3. 通常来说，用户态能节省网络上的CPU损耗。在CPU资源充足情况下，用户态提升会不明显。